FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND="noninteractive"
ENV PYENV_ROOT="/.pyenv" PATH="/.pyenv/bin:/.pyenv/shims:$PATH"

COPY python-versions.txt ./

# not all recommended build deps installed to reduce bloat, for a complete list
# see https://github.com/pyenv/pyenv/wiki#suggested-build-environment
RUN apt-get update \
    && apt-get -y install --no-install-recommends \
        ca-certificates curl git libsqlite3-0 \
        build-essential libbz2-dev libffi-dev libreadline-dev libsqlite3-dev \
        libssl-dev xz-utils zlib1g-dev \
    && curl -sS https://pyenv.run | bash \
    && xargs -P 4 -n 1 pyenv install < python-versions.txt \
    && find $PYENV_ROOT/versions -depth -name '__pycache__' -type d \
        -exec rm -rf '{}' + \
    && find $PYENV_ROOT/versions -depth -name '*.py[co]' -type f \
        -exec rm -f '{}' + \
    && apt-get --autoremove -y purge \
        build-essential libbz2-dev libffi-dev libreadline-dev libsqlite3-dev \
        libssl-dev xz-utils zlib1g-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pyenv global $(pyenv versions --bare) \
    && mv python-versions.txt $PYENV_ROOT/version

# install pip packages for each python version
RUN for PV in $(pyenv version-name |cut --output-delimiter=' ' -d: -f1-); do \
        pyenv local $PV \
        && python -m pip install --upgrade pip \
        && python -m pip install \
            coverage[toml] pylint-json2html tox tox-factor wheel \
    ; done

# enable all python verisons
RUN pyenv local $(pyenv versions --bare |sort -V)
