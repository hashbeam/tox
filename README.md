Tox CI docker image
===

Docker image to run tests on multiple python3 versions using
[tox](https://github.com/tox-dev/tox).

[Pyenv](https://github.com/pyenv/pyenv) is used to manage multiple python
versions.

## Python versions

Python versions are taken from the `python-versions.txt` file.

To update those versions to the latest point release, run (requires pyenv):
```bash
$ ./update-versions.sh
```

## Image variants

This image is available in two variants: a base and a docker one. The base
variant does not include Docker, while the docker variant has it installed. The
main purpose of the docker version is to run tests requiring a docker-in-docker
setup.

The name of the docker image variant includes a `/docker` suffix.

Pre-built images of both variants can be downloaded from [GitLab's container
registry](https://gitlab.com/hashbeam/tox/container_registry/).

## Build

To build the base image variant run:
```bash
$ docker build -t registry.gitlab.com/hashbeam/tox .
```

To build the docker image variant run:
```bash
$ docker build -f Dockerfile.docker -t registry.gitlab.com/hashbeam/tox/docker .
```

## Run

To run tests using this image, make the code to be tested available to the
container by mounting it and call `pyenv exec tox` from the directory where
the code is found.

GitLab CI will automatically make the project code available and spawn shells
where the code has been cloned, so calling tox should be enough.
