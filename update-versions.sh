#!/bin/bash
#
# (re)generate python versions file with latest patch versions for each listed
# minor

VER_FILE="python-versions.txt"
MINOR_VERS="7 8 9 10"

rm -f $VER_FILE
for M in $MINOR_VERS; do
    pyenv install -l |awk "/ 3\.$M/ {ver=\$1} END {print ver}" >> $VER_FILE
done
